#!/bin/bash

1 Vous devez entrer “man mkdir”.

__Avec man mkdir est le manuel de la commande qui permet les créations des dossiers.


2 Vous devez créer 3 répertoires “rep-toto”, “rep-titi”, “rep-tata” en imbriqué.

___Pour créer les  3 répertoires la commande est mkdir + les noms de fichiers indiqués.


3 Vous devez créer 3 fichiers “toto”, “titi”, “tata” dans chacun des répertoires que vous venez de créer.

__par exemple : je rentre dans chaque dossier avec « cd  rep-toto puis je crée un fichier avec touch = nom du fichier.odt.

En suite je sors du dossier avec   ../   puis je renouvelle la commande pour les autres dossiers.

Puis je vérifie avec la commande si le fichier a bien été créée.


4 Vous devez créer un symlink portant le nom de 42 pour accéder au répertoire rep-toto que vous venez de créer.

__Je tape la la commande suivante : ln -s rep-toto 42


5 Vous devez lister les informations des répertoires avec une commande vous permettant
de voir les fichiers cachés et permettre une lecture humaine du poids de chaque fichier.

__Je tape la la commande suivante : ls -la


6 Vous devez installer “oh-my-zsh”.

__Je tape tout d’abord « sudo apt update pour la mise à jour du système puis 
installer git et enfin lancer cette commande :

	sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"




7 devez installer le template “robbyrussell” avec “oh-my-zsh”.

__La commande précédente permet d'installer le template  « robbyrussell » grace au lien de Git".


8 Vous devez installer vim.

__Je tape la commande suivante : sudo apt-get install vim 


9 Vous devez définir “vim” comme éditeur par défaut


10 Vous devez installer “git”.

__sudo apt install git


11 Vous devez configurer les short cuts “ci”, “co” ,“br” et “st” pour GIT.

J’exécute les commande ci dessous dans le fichier .giconfig

__$ git config --global alias.ci commit → ci
__$ git config --global alias.co branch→ co
__$ git config --global alias.br branch → br
__$ git config --global alias.st branch → st


2 Vous devez copier tous les répertoires dans “rep-toto” vers un nouveau répertoire 42-cpy.

__Je fais : mkdir  42-cpy puis cp -r  rep-toto  42-cpy 


13 Vous devez renommer le dossier “42-cpy” vers “42-cpy-rename”.
__mv 42-cpy 42-cpy-rename


 14 Vous devez copier “42-cpy-rename” vers “42-cpy-rename-v2” puis supprimer le repertoire “42-cpy-rename”.

__mkdir 42-cpy-rename-v2 | cp -r 42-cpy-rename 42-cpy-rename-v2 

Puis je fais  rm -r 42-cpy-rename


15  Vous devez faire en sorte que votre répertoire “42-cpy-rename-v2” est la sortie : drwxr-xr-x

__Je vérifie le dossier indiqué avec ls -a -l et je constate que 42-cpy-rename-v2 a une sortie drwxr-xr-x


16 Vous devez créer un nouvel utilisateur bot42.
__sudo adduser bot42


17 Vous devez donner les droits root à bot42.
__Apres ma recherche sur Google je l’ai fait manuellement avec 
sudo visudo puis dans le fichier /etc/sudoers je mets 
bot42 ALL=(ALL) ALL.


18 Vous devez créer un répertoire “root-42” avec le super user.
__je me mets dan home/bot42 puis je fais sudo mkdir root-42


19 Vous devez supprimer le répertoire “root-42” avec l’utilisateur bot42.
__sudo  rm -rf root-42


20 Vous devez obtenir le résultat suivant sur le fichier “42-letter-count”: 42 42-letter-count
__sudo  touch 42 42-letter-count


21 Créer le fichier “42-user” avec le super user puis changez les droits utilisateurs de root vers bot42.
__ touch 42-user


22 Vous devez créer un fichier “my-file-42” et utiliser un “ls” avec la commande “cut” pour ne faire apparaître que les permissions “-rw-r--r--”
__$ ls -la cut


23 Vous devez créer une tâche récurrente qui va créer une ligne de log “i love 42” toutes les minutes dans un fichier “my-log-42.log”

__ Je fais touch my-log-42.log puis j’édite la récurrence avec crontab -e
et dans crontab je fais 
* * * * *  i love 42 >> my-log-42.log


24 Vous devez afficher l’intégralité d’un fichier de log et voir en temps réel l’ajout de nouvelles lignes sur le fichier “my-log-42.log” dans votre terminal.

__ crontab -l


25 Créez le fichier “toto-42” et éditez le pour lui faire prendre un poids de “42 ko”.

__sudo dd if=/dev/zero of=toto-42 bs=42 count=1000
Puis j’affiche pour vérifier 
__ ls -al toto-42                                   
-rw-r--r-- 1 root root 42000 avril  2 15:07 toto-42


26 Vous devez changer les permissions du fichier “toto-42” pour lui donner les droits suivants : “rw---x-wx”

__ sudo chmod 613 toto-42


27 Ouvrez “Google Chrome” et trouvez le moyen de fermer “google chrome”.

__ Je tape google-chrome dans mon terminal puis pour fermer  wmctrl -c chrome


28 Vous classez en temps réel la prise en charge CPU de tous les processus de votre système.

__htop


29 Vous devez compter le nombre de ligne dans le fichier “/etc/passwd”

__ wc -l /etc/passwd                             
résultat -> 44 /etc/passwd


30 Vous devez télécharger la page “https://www.google.fr” dans un fichier index.html

__ Je fais sudo touch index.html  puis chmod a+x index.html
Ensuite wget -P $HOME/bot42/index.html https://www.google.fr

